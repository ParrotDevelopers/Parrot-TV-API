// ------------------------------------------------------------------------------
//                                 SK Channels
// ------------------------------------------------------------------------------

var joj = function joj(){
    var player = new Playerjs({id:"player", file:"//nn2.joj.sk/hls/joj-720.m3u8"});

}

var jojplus = function jojplus(){
    var player = new Playerjs({id:"player", file:"//nn2.joj.sk/hls/jojplus-540.m3u8"});

}

var jojfamily = function jojfamily(){
    var player = new Playerjs({id:"player", file:"//nn.geo.joj.sk/hls/family-360.m3u8"});

}

var wau = function wau(){
    var player = new Playerjs({id:"player", file:"https://nn.geo.joj.sk/live/wau-index.m3u8?fluxustv.m3u8"});

}

var markiza = function markiza(){
    var player = new Playerjs({id:"player", file:"https://list.iptvcat.com/my_list/s/fc11d89f9c1bcfeb43a472f39f422970.m3u8"});
}

var ta3 = function ta3(){
  alert("Currently not working")
    var player = new Playerjs({id:"player", file:"https://tv.parrottv.tk/TA3.m3u8"});

}

var fashiontv = function fashiontv(){
    var player = new Playerjs({id:"player", file:"//lb.streaming.sk/fashiontv/stream/playlist.m3u8?fluxustv.m3u8"});

}

var rik = function rik(){
    var player = new Playerjs({id:"player", file:"https://nn.geo.joj.sk/live/hls/rik-540.m3u8"});

}

var tvlux = function tvlux(){
    var player = new Playerjs({id:"player", file:"//live.tvlux.sk:1935/lux/lux.stream_360p/chunklist_w1295439472.m3u8"});

}

var lifetv = function lifetv(){
    var player = new Playerjs({id:"player", file:"https://lifetv.mpks.sk/s.m3u8"});

}

var senzitv = function senzitv(){
    var player = new Playerjs({id:"player", file:"//109.74.144.130/live/senzitest/playlist.m3u8"});

}

var tvosem = function tvosem(){
    var player = new Playerjs({id:"player", file:"//109.74.145.11:1935/tv8/ngrp:tv8.stream_all/playlist.m3u8"});

}

var tvraj = function tvraj(){
    var player = new Playerjs({id:"player", file:"//ottst05.flexitv.sk/2827-tv-pc.m3u8"});

}

var btv = function btv(){
    console.log("RTMP Not Supported");
    console.log(btv);
    var player = new Playerjs({id:"player", file:"rtmp://s1.media-planet.sk:80/live/bardejov1"});

}

var kd = function kd(){
    var player = new Playerjs({id:"player", file:"//lb.streaming.sk/tvnasa/stream/playlist.m3u8"});

}

var tvnz = function tvnz(){
    var player = new Playerjs({id:"player", file:"//s1.media-planet.sk:80/live/novezamky/BratuMarian.m3u8"});

}

var tvr = function tvr(){
    console.log("RTMP Not Supported");
    console.log(tvr);
    var player = new Playerjs({id:"player", file:"rtmp://s1.media-planet.sk:80/live/reduta"});

}

var tvru = function tvru(){
    var player = new Playerjs({id:"player", file:"//lb.streaming.sk/tvruzinov/stream/playlist.m3u8"});

}

var tvt = function tvt(){
    console.log("RTMP Not Supported");
    console.log(tvt);
    var player = new Playerjs({id:"player", file:"rtmp://s1.media-planet.sk:80/live/turzovka"});

}







// ------------------------------------------------------------------------------
//                                 CZ Channels
// ------------------------------------------------------------------------------

var nova = function nova(){
    var player = new Playerjs({id:"player", file:"https://nova-live.ssl.cdn.cra.cz/channels/nova_avod/playlist/cze/live_hq.m3u8"});

}

var nova2 = function nova2(){
    var player = new Playerjs({id:"player", file:"https://nova-live.ssl.cdn.cra.cz/channels/nova_2_avod/playlist.m3u8"});

}

var novagold = function novagold(){
    var player = new Playerjs({id:"player", file:"https://nova-live.ssl.cdn.cra.cz/channels/nova_gold_avod/playlist.m3u8"});

}

var novaaction = function novaaction(){
    var player = new Playerjs({id:"player", file:"//rtmp.elektrika.cz/live/myStream.sdp/playlist.m3u8"});

}

var novacinema = function novacinema(){
    var player = new Playerjs({id:"player", file:"https://nova-live.ssl.cdn.cra.cz/channels/nova_cinema_avod/playlist/cze/live_hq.m3u8"});

}

var ct1 = function ct1(){
  var player = new Playerjs({id:"player", file:"http://213.151.233.20:8000/dna-5100-tv-pc/hls/4002v105.m3u8"});
}

var elektrikatv = function elektrikatv(){
    var player = new Playerjs({id:"player", file:"//rtmp.elektrika.cz/live/myStream.sdp/playlist.m3u8"});

}

var ocko = function ocko(){
    var player = new Playerjs({id:"player", file:"//ocko-live.ssl.cdn.cra.cz/channels/ocko/playlist.m3u8"});

}

var ockogold = function ockogold(){
    var player = new Playerjs({id:"player", file:"//ocko-live.ssl.cdn.cra.cz/channels/ocko_gold/playlist/cze/live_hq.m3u8"});

}

var ockoexpres = function ockoexpres(){
    var player = new Playerjs({id:"player", file:"//ocko-live.ssl.cdn.cra.cz/channels/ocko_expres/playlist/cze/live_hq.m3u8"});

}

var ockostar = function ockostar(){
    var player = new Playerjs({id:"player", file:"//ocko-live.ssl.cdn.cra.cz/channels/ocko_gold/playlist/cze/live_hq.m3u8"});

}

var polartv = function polartv(){
    var player = new Playerjs({id:"player", file:"https://stream.polar.cz/polar/polarlive-1/playlist.m3u8"});

}

var huntertv = function huntertv(){
    var player = new Playerjs({id:"player", file:"//www.huntertv.cz/live/4-playlist.m3u8"});

}

var prahatv = function prahatv(){
    var player = new Playerjs({id:"player", file:"https://stream.polar.cz/prahatv/prahatvlive-1/playlist.m3u8"});

}

var tvnoe = function tvnoe(){
    var player = new Playerjs({id:"player", file:"https://w100.quickmedia.tv/prozeta-live04/_definst_/prozeta-live04.smil/Playlist.m3u8"});

}

var retro = function retro(){
    var player = new Playerjs({id:"player", file:"//stream.mediawork.cz/retrotv/retrotvHQ1/playlist.m3u8"});

}

var slagr = function slagr(){
    var player = new Playerjs({id:"player", file:"https://slagrtv-live-hls.ssl.cdn.cra.cz/channels/slagrtv/playlist/cze/live_hq.m3u8"});

}

var slagr2 = function slagr2(){
    var player = new Playerjs({id:"player", file:"//92.62.234.233/slagr2.m3u"});

}

var tvnatura = function tvnatura(){
    var player = new Playerjs({id:"player", file:"//media1.tvnatura.cz/live_out/1/live.m3u8"});

}

var vctv = function vctv(){
    var player = new Playerjs({id:"player", file:"https://stream.polar.cz/vctv/vctvlive-1/playlist.m3u8"});

}

var tvm = function tvm(){
    var player = new Playerjs({id:"player", file:"https://hlscat.com/my_list/s/f0f7d1cae255f3d243cafe2f4764c9d5.m3u8"});

}



// ------------------------------------------------------------------------------
//                                Ekostol.sk
// ------------------------------------------------------------------------------

var trencin = function trencin(){
    var player = new Playerjs({id:"player", file:"https://kukaj.profi-net.sk:443/commercial/KOSTOLTRENCIN.stream/playlist.m3u8"});

}

var ruzomberok = function ruzomberok(){
    var player = new Playerjs({id:"player", file:"https://kukaj.profi-net.sk:443/commercial/KOSTOLRK.stream/playlist.m3u8"});

}

var spis = function spis(){
    var player = new Playerjs({id:"player", file:"https://kukaj.profi-net.sk:443/commercial/KOSTOLSPISSKYSTVRTOK.stream/playlist.m3u8"});

}

var hornazdana = function hornazdana(){
    var player = new Playerjs({id:"player", file:"https://kukaj.profi-net.sk:443/commercial/KOSTOLHORNAZDANA.stream/playlist.m3u8"});

}

var partizanske = function partizanske(){
    var player = new Playerjs({id:"player", file:"https://kukaj.profi-net.sk:443/commercial/KOSTOLPARTIZANSKE.stream/playlist.m3u8"});

}

var kosicesaca = function kosicesaca(){
    var player = new Playerjs({id:"player", file:"https://kukaj.profi-net.sk:443/commercial/KOSTOLKOSICESACA.stream/playlist.m3u8"});

}

var ludanice = function ludanice(){
    var player = new Playerjs({id:"player", file:"https://kukaj.profi-net.sk:443/commercial/KOSTOLLUDANICE.stream/playlist.m3u8"});

}

var radosina = function radosina(){
    var player = new Playerjs({id:"player", file:"https://stream.wircom.sk:443/kos/pan.stream/playlist.m3u8"});

}

var vnz = function vnz(){
    var player = new Playerjs({id:"player", file:"https://kukaj.profi-net.sk:443/commercial/KOSTOLNVNZ.stream/playlist.m3u8"});

}

var bojna = function bojna(){
    var player = new Playerjs({id:"player", file:"https://stream.wircom.sk:443/bojna//bojna.stream/playlist.m3u8"});

}

var cernova = function cernova(){
    var player = new Playerjs({id:"player", file:"https://kukaj.profi-net.sk:443/commercial/KOSTOLCERNOVA.stream/playlist.m3u8"});

}

var janovce = function janovce(){
    var player = new Playerjs({id:"player", file:"https://kukaj.profi-net.sk:443/commercial/KOSTOLJANOVCE.stream/playlist.m3u8"});

}

var lukacovce = function lukacovce(){
    var player = new Playerjs({id:"player", file:"https://kukaj.profi-net.sk:443/commercial/KOSTOLLUKACOVCE.stream/playlist.m3u8"});

}

var ruzomberok = function ruzomberok(){
    var player = new Playerjs({id:"player", file:"https://kukaj.profi-net.sk:443/commercial/KOSTOLJEZUITI.stream/playlist.m3u8"});

}