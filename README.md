## Parrot TV - PlayerJS API
Parrot TV API for websites.

## Usage
Copy and paste this into your head tag.

```html
<script src="https://parrotdevelopers.github.io/Parrot-TV-API/ChannelSelect.js" type="text/javascript"></script>
```

## Help
1. Must use PlayerJS with default id or it wont work.

2. Just check the file and copy function then paste it into button. 

## Example
```html
<a class="channels" onclick="jojplus()"><button><img border="0" src="https://i.ibb.co/G5VWSNM/Plus.png" width="" height="30px"></button></a>
```
